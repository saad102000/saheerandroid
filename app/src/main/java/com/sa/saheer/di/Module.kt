package com.sa.saheer.di

import android.content.Context
import com.google.gson.GsonBuilder
import com.sa.saheer.BuildConfig
import com.sa.saheer.data.Repository
import com.sa.saheer.data.RepositoryImp
import com.sa.saheer.data.local.PreferencesHelperImp
import com.sa.saheer.data.remote.NetworkConnectionInterceptor
import com.sa.saheer.data.remote.RemoteRepository
import com.sa.saheer.data.remote.RemoteRepositoryImp
import com.sa.saheer.data.remote.ServiceApi
import com.sa.saheer.ui.addAttendance.AddAttendanceViewModel
import com.sa.saheer.ui.dailyReport.DailyReportViewModel
import com.sa.saheer.ui.login.LoginViewModel
import com.sa.saheer.ui.registerAttendance.RegisterAttendanceViewModel
import com.sa.saheer.ui.vacationRequest.VacationRequestViewModel
import com.sa.saheer.data.local.PreferencesHelper
import com.sa.saheer.ui.home.HomeViewModel
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.util.concurrent.TimeUnit


val viewModelModule = module {
    viewModel { HomeViewModel(repository = get()) }
    viewModel { LoginViewModel(repository = get()) }
    viewModel { AddAttendanceViewModel(repository = get()) }
    viewModel { DailyReportViewModel(repository = get()) }
    viewModel { RegisterAttendanceViewModel(repository = get()) }
    viewModel { VacationRequestViewModel(repository = get()) }

}

val repositoryModule = module {
    single<RemoteRepository> { RemoteRepositoryImp(get()) }
    single<PreferencesHelper> { PreferencesHelperImp(get()) }
    single<Repository> { RepositoryImp(get(), get()) }
}

val apiModule = module {
    fun provideApiModule(retrofit: Retrofit): ServiceApi {
        return retrofit.create(ServiceApi::class.java)
    }

    single { provideApiModule(get()) }

}
val retrofitModule = module {

/*    fun provideGson(): CallAdapter.Factory {
        return RxJava2CallAdapterFactory.create()
    }*/

    fun provideCacheFile(context: Context): File {
        return File(context.cacheDir, "okHttp_cache")
    }

    fun provideCache(cacheFile: File): Cache {
        return Cache(cacheFile, 10 * 1024 * 1024)
    }

    fun provideLoggingInterceptor(): HttpLoggingInterceptor {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        return interceptor
    }

    fun provideNetworkConnInterceptor(context: Context, preferencesHelper: PreferencesHelper): NetworkConnectionInterceptor {
        return NetworkConnectionInterceptor(context, preferencesHelper)
    }

    fun provideHttpClient(
        cache: Cache,
        loggingInterceptor: HttpLoggingInterceptor,
        networkInterceptor: NetworkConnectionInterceptor
    ): OkHttpClient {
        val builder = OkHttpClient.Builder()
            .cache(cache)
            .addInterceptor(networkInterceptor)
            .connectTimeout(100, TimeUnit.SECONDS)
            .writeTimeout(100, TimeUnit.SECONDS)
            .readTimeout(100, TimeUnit.SECONDS)

        if (BuildConfig.DEBUG) {
            builder.addInterceptor(loggingInterceptor)
        }
        return builder.build()
    }

    fun provideRetrofit(client: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .addConverterFactory(
                GsonConverterFactory.create(
                    GsonBuilder().setLenient().create()
                )
            )
            .client(client)
            .build()
    }

    single { provideCacheFile(androidContext()) }
    single { provideCache(get()) }
    single { provideLoggingInterceptor() }
    single { provideNetworkConnInterceptor(androidContext(), get()) }

    single {
        provideHttpClient(
            cache = get(),
            loggingInterceptor = get(),
            networkInterceptor = get()
        )
    }
    single { provideRetrofit(client = get()) }
}
