package com.sa.saheer.data.remote.models

data class AttendanceRequest(
    val attendanceTime: String,
    val faceImage: String,
    val latitude: Double,
    val longitude: Double
)