package com.sa.saheer.data

import com.sa.saheer.data.local.PreferencesHelper
import com.sa.saheer.data.remote.RemoteRepository
import com.sa.saheer.data.remote.models.AbsenceRequest
import com.sa.saheer.data.remote.models.AttendanceRequest
import com.sa.saheer.data.remote.models.SubmitLeaveRequest

open class RepositoryImp(
    private val remoteDataSource: RemoteRepository,
    private val prefDataSource: PreferencesHelper
) : Repository {
    //region remote
    override suspend fun login(userId: Long, pw: String) = remoteDataSource.login(userId, pw)

    override suspend fun attendanceCheckIn(userId: Long, request: AttendanceRequest) =
        remoteDataSource.attendanceCheckIn(userId, request)

    override suspend fun attendanceCheckOut(userId: Long, request: AttendanceRequest) =
        remoteDataSource.attendanceCheckOut(userId, request)

    override suspend fun submitLeave(userId: Long, request: SubmitLeaveRequest) =
        remoteDataSource.submitLeave(userId, request)

    override suspend fun getLeaveTypes() =
        remoteDataSource.getLeaveTypes()

    override suspend fun getSupervisorGuards(supervisorId: Long) =
        remoteDataSource.getSupervisorGuards(supervisorId)

    override suspend fun attendanceReport(userId: Long, from: String, to: String) =
        remoteDataSource.attendanceReport(userId, from, to)

    override suspend fun absence(request: AbsenceRequest) = remoteDataSource.absence(request)
    //endregion

    //region  PrefDataSource
    override fun setJwt(jwt: String) = prefDataSource.setJwt(jwt)
    override fun getJwt() = prefDataSource.getJwt()
    override fun setUserId(userId: Long) = prefDataSource.setUserId(userId)
    override fun getUserId() = prefDataSource.getUserId()
    override fun setUserRole(role: String) = prefDataSource.setUserRole(role)
    override fun getUserRole() = prefDataSource.getUserRole()
    override fun setUserName(displayName: String) = prefDataSource.setUserName(displayName)
    override fun getUserName() = prefDataSource.getUserName()

    override fun setPw(pw: String) = prefDataSource.setPw(pw)
    override fun getPw() = prefDataSource.getPw()
    //endregion

}
