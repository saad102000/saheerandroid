package com.sa.saheer.data.remote

import com.sa.saheer.data.remote.models.*
import retrofit2.Response
import retrofit2.http.Body

interface RemoteRepository {
    suspend fun login(userId: Long, pw: String): Result<LoginResponse>
    suspend fun attendanceCheckIn(userId: Long, request: AttendanceRequest): Result<Void>
    suspend fun attendanceCheckOut(userId: Long, request: AttendanceRequest): Result<Void>
    suspend fun submitLeave(userId: Long, request: SubmitLeaveRequest): Result<Void>
    suspend fun getLeaveTypes(): Result<ArrayList<LookupItem>>
    suspend fun getSupervisorGuards(supervisorId: Long): Result<ArrayList<LookupItem>>
    suspend fun attendanceReport(userId: Long, from: String, to: String): Result<ArrayList<AttendanceItem>>
    suspend fun absence(request: AbsenceRequest): Result<LoginResponse>
}