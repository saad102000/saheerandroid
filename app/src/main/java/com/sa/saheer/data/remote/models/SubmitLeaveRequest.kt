package com.sa.saheer.data.remote.models

data class SubmitLeaveRequest(
    val startDate: String,
    val endDate: String,
    val type: String,
    val note: String
)