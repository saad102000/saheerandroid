package com.sa.saheer.data.remote.models

data class LoginRequest(
    val username: Long,
    val password: String
)