package com.sa.saheer.data

import com.sa.saheer.data.local.PreferencesHelper
import com.sa.saheer.data.remote.RemoteRepository

interface Repository : RemoteRepository, PreferencesHelper


