package com.sa.saheer.data.remote

import com.sa.saheer.data.remote.models.*
import retrofit2.Response
import retrofit2.http.*

interface ServiceApi {

    @POST("account/authentication")
    suspend fun login(@Body request: LoginRequest): Response<LoginResponse>

    @POST("users/{userId}/attendance/check-in")
    suspend fun attendanceCheckIn(
        @Path("userId") userId: Long,
        @Body request: AttendanceRequest
    ): Response<Void>

    @POST("users/{userId}/attendance/check-out")
    suspend fun attendanceCheckOut(
        @Path("userId") userId: Long,
        @Body request: AttendanceRequest
    ): Response<Void>

    @POST("users/{userId}/leaves")
    suspend fun submitLeave(
        @Path("userId") userId: Long,
        @Body request: SubmitLeaveRequest
    ): Response<Void>

    @GET("users/{userId}/attendance")
    suspend fun attendanceReport(
        @Path("userId") userId: Long,
        @Query("from") from: String,
        @Query("tp") to: String,
    ): Response<ArrayList<AttendanceItem>>

    @GET("lookups/leave-types")
    suspend fun getLeaveTypes(): Response<ArrayList<LookupItem>>

    @GET("supervisors/{supervisorId}/guards")
    suspend fun getSupervisorGuards(@Path("supervisorId") supervisorId: Long): Response<ArrayList<LookupItem>>

    @POST("/api/supervisors/absence")
    suspend fun absence(@Body request: AbsenceRequest): Response<LoginResponse>
}