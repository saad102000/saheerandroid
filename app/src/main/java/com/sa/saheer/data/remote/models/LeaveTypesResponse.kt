package com.sa.saheer.data.remote.models

data class LookupItem(
    val id: Long,
    val name: String
)