package com.sa.saheer.data.remote.models

import androidx.annotation.Keep

@Keep
data class LoginResponse(
    val employeeId: Long,
    val fullname: String,
    val jwt: String,
    val role: String
)