package com.sa.saheer.data.remote

import com.sa.saheer.data.remote.models.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response


class RemoteRepositoryImp(private val serviceApi: ServiceApi) : RemoteRepository {
    override suspend fun login(userId: Long, pw: String) = withContext(Dispatchers.IO) {
        safeApiResult(serviceApi.login(LoginRequest(userId, pw)))
    }

    override suspend fun attendanceCheckIn(userId: Long, request: AttendanceRequest) =
        withContext(Dispatchers.IO) {
            safeApiResult(serviceApi.attendanceCheckIn(userId, request))
        }

    override suspend fun attendanceCheckOut(userId: Long, request: AttendanceRequest) =
        withContext(Dispatchers.IO) {
            safeApiResult(serviceApi.attendanceCheckOut(userId, request))
        }

    override suspend fun submitLeave(userId: Long, request: SubmitLeaveRequest) =
        withContext(Dispatchers.IO) {
            safeApiResult(serviceApi.submitLeave(userId, request))
        }

    override suspend fun getLeaveTypes() =
        withContext(Dispatchers.IO) {
            safeApiResult(serviceApi.getLeaveTypes())
        }

    override suspend fun getSupervisorGuards(supervisorId: Long) =
        withContext(Dispatchers.IO) {
            safeApiResult(serviceApi.getSupervisorGuards(supervisorId))
        }

    override suspend fun attendanceReport(userId: Long, from: String, to: String) =
        withContext(Dispatchers.IO) {
            safeApiResult(serviceApi.attendanceReport(userId, from, to))
        }

    override suspend fun absence(request: AbsenceRequest) =
        withContext(Dispatchers.IO) {
            safeApiResult(serviceApi.absence(request))
        }


    private fun <T> safeApiResult(call: Response<T>): Result<T> {
        if (call.isSuccessful) return Result.Success(call.body())

        when (call.code()) {
            500, 504 -> throw Failure.ServerException(call.message())
            404 -> throw Failure.NotFoundException(call.message())
            401 -> throw Failure.UnauthorizedException(call.message())
            400 -> throw Failure.BadRequestException(call.message())
            else -> {
                return Result.Error(Failure.UnknownException(call.message()))
            }
        }
    }
}