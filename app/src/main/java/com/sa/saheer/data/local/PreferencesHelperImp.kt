package com.sa.saheer.data.local

import android.content.Context
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKey

class PreferencesHelperImp(private val context: Context) : PreferencesHelper {

    var masterKey = MasterKey.Builder(context, MasterKey.DEFAULT_MASTER_KEY_ALIAS)
        .setKeyScheme(MasterKey.KeyScheme.AES256_GCM).build()

    private val sharedPreferences =
        EncryptedSharedPreferences.create(
            context,
            PREF_NAME,
            masterKey,
            EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
            EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
        )

    companion object {
        private const val PREF_NAME = "saher_pref"
        private const val SESSION_JWT = "guest_jwt"
        private const val Emp_ID = "emp_id"
        private const val USER_ROLE = "user_role"
        private const val USER_PW = "user_pw"
        private const val USER_NAME = "user_name"
    }

    override fun setJwt(jwt: String) = setStringPreference(SESSION_JWT, jwt)

    override fun getJwt() = getStringPreference(SESSION_JWT)

    override fun setUserId(userId: Long) = setLongPreference(Emp_ID, userId)

    override fun getUserId() = getLongPreference(Emp_ID)

    override fun setUserRole(role: String) = setStringPreference(USER_ROLE, role)

    override fun getUserRole() = getStringPreference(USER_ROLE)

    override fun setUserName(displayName: String) = setStringPreference(USER_NAME, displayName)

    override fun getUserName() = getStringPreference(USER_NAME)

    override fun setPw(pw: String) = setStringPreference(USER_PW, pw)

    override fun getPw() = getStringPreference(USER_PW)

    private fun setStringPreference(key: String, value: String) =
        sharedPreferences.edit().putString(key, value)
            .apply()


    private fun getStringPreference(key: String, defaultValue: String? = null) =
        sharedPreferences
            .getString(key, defaultValue)


    private fun setIntPreference(key: String, value: Int) =
        sharedPreferences.edit().putInt(key, value)
            .apply()


    private fun getIntPreference(key: String, defaultValue: Int = -1) =
        sharedPreferences.getInt(key, defaultValue)

    private fun setLongPreference(key: String, value: Long) =
        sharedPreferences.edit().putLong(key, value)
            .apply()


    private fun getLongPreference(key: String, defaultValue: Long = -1) =
        sharedPreferences.getLong(key, defaultValue)


}