package com.sa.saheer.data.remote.models

import com.sa.saheer.utils.SERVER_DATE_TIME_FORMAT
import java.text.SimpleDateFormat
import java.util.*

data class AttendanceItem(
    val attendanceTime: String,
    val id: Long,
    val isCheckIn: Boolean
){
    fun getAttendanceTimeCal(): Calendar {
        val cal = Calendar.getInstance()
        SimpleDateFormat(SERVER_DATE_TIME_FORMAT, Locale.ENGLISH).parse(attendanceTime)?.let {
            cal.timeInMillis = it.time
        }
        return cal
    }
}