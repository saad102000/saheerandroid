package com.sa.saheer.data.remote.models

data class AbsenceRequest(
    val absenceDateTime: String,
    val guardId: Long,
    val notes: String
)