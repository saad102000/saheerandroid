package com.sa.saheer.data.remote

import android.content.Context
import android.content.SharedPreferences
import com.sa.saheer.R
import com.sa.saheer.data.local.PreferencesHelper
import com.sa.saheer.utils.NoInternetException
import com.sa.saheer.utils.isNetworkConnected
import okhttp3.Interceptor
import okhttp3.Response

class NetworkConnectionInterceptor(private val context: Context, private val preferences: PreferencesHelper) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        if (!context.isNetworkConnected()) {
            throw NoInternetException(context.getString(R.string.msg_no_internet))
        }

        val request = chain.request().newBuilder()
            .addHeader("Content-Type", "application/json")
            .addHeader("Accept", "application/json")
            .addHeader("Authorization", "Bearer " + preferences.getJwt())

        return chain.proceed(request.build())
    }
}