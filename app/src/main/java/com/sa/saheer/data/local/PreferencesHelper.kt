package com.sa.saheer.data.local

interface PreferencesHelper {
    fun setJwt(jwt: String)
    fun getJwt(): String?

    fun setUserId(userId: Long)
    fun getUserId(): Long

    fun setUserRole(role: String)
    fun getUserRole(): String?

    fun setUserName(displayName: String)
    fun getUserName(): String?

    fun setPw(pw: String)
    fun getPw(): String?


}