package com.sa.saheer.utils


import android.Manifest
import android.app.Activity
import android.content.Intent
import android.provider.Settings
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import com.karumi.dexter.Dexter
import com.karumi.dexter.listener.OnDialogButtonClickListener
import com.karumi.dexter.listener.single.BasePermissionListener
import com.karumi.dexter.listener.single.CompositePermissionListener
import com.karumi.dexter.listener.single.DialogOnDeniedPermissionListener
import com.sa.saheer.R


object PermissionsHelper {

    fun checkForLocationPermission(activity: Activity, listener: BasePermissionListener?) {

        val dialogPermissionListener = DialogOnDeniedPermissionListener.Builder
            .withContext(activity)
            .withTitle(R.string.permission_fine_location_title)
            .withMessage(R.string.permission_fine_location_message)
            .withButtonText(android.R.string.ok) { checkForLocationPermission(activity, listener) }
            .withIcon(ContextCompat.getDrawable(activity, R.drawable.ic_baseline_location_on_24))
            .build()

        val compositeListener =
            if (listener != null) {
                CompositePermissionListener(dialogPermissionListener, listener)
            } else {
                CompositePermissionListener(dialogPermissionListener)
            }

        Dexter.withContext(activity)
            .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
            .withListener(compositeListener)
            .onSameThread()
            .check()
    }


    fun showSettingsAlert(activity: Activity) {
        val alertDialog: AlertDialog.Builder = AlertDialog.Builder(activity)

        // Setting Dialog Title
        alertDialog.setTitle(activity.getString(R.string.gps_required))

        // Setting Dialog Message
        alertDialog.setMessage(R.string.gps_enable_msg)

        // On pressing Settings button
        alertDialog.setPositiveButton(
            R.string.settings
        ) { dialog, which ->
            val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
            activity.startActivity(intent)
        }

        // on pressing cancel button
        alertDialog.setNegativeButton(R.string.cancel) { dialog, which -> dialog.cancel() }

        // Showing Alert Message
        alertDialog.show()
    }
}
