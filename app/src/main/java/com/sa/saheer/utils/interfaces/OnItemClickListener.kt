package com.sa.saheer.utils.interfaces

interface OnItemClickListener<T> {
    fun onItemClicked(item: T)
}