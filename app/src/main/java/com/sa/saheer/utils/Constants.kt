package com.sa.saheer.utils

import android.os.Build
import androidx.annotation.Keep
import java.text.SimpleDateFormat
import java.util.*


@Keep
enum class EmpRole(val role: String) {
    GUARD("Guard"),
    SUPERVISOR("Supervisor"),
    ADMIN("Admin")
}

const val SERVER_DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS"
const val UI_DATE_FORMAT = "yyyy/MM/dd"
const val UI_TIME_FORMAT = "h:mm a"
const val UI_DATE_TIME_FORMAT = "yyyy/MM/dd - h:mm a"
const val PERMISSION_ID = 5
const val IS_24_HOURS = false
const val MAX_TIME_DIFFERENCE = 5


val DEVICE_ID: String
    get() {
        return "2021aa-" +
                Build.BOARD.length % 10 + Build.BRAND.length % 10 + "ee-" +
                Build.DEVICE.length % 10 + Build.USER.length % 10 + "cc-" +
                Build.DISPLAY.length % 10 + Build.HOST.length % 10 + "dd-" +
                Build.ID.length % 10 + Build.MANUFACTURER.length % 10 + Build.MODEL.length % 10 +
                "c29" + Build.PRODUCT.length % 10 + Build.TAGS.length % 10 + Build.TYPE.length % 10 + "ddg"
    }

fun getCurrentTimeMs() = System.currentTimeMillis()
fun getCurrentTimeMsServerFormat(timeMs: Long = getCurrentTimeMs()) =
    SimpleDateFormat(SERVER_DATE_TIME_FORMAT, Locale.ENGLISH).format(timeMs)

/*@Keep
enum class RegistrationType(type: Pair<String, Int>) {
    POSTPAID("POSTPAID"),
    PREPAID("PREPAID"),
    UNKNOWN("UNKNOWN")
}*/





