package com.sa.saheer

import android.util.Log
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.akexorcist.localizationactivity.ui.LocalizationApplication
import com.instacart.library.truetime.TrueTimeRx
import com.sa.saheer.di.apiModule
import com.sa.saheer.di.repositoryModule
import com.sa.saheer.di.retrofitModule
import com.sa.saheer.di.viewModelModule
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.*
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import java.util.*

class SaherApp : LocalizationApplication(), LifecycleObserver {
    override fun getDefaultLanguage() = Locale("ar")

    private val TAG: String = "SaherApp"
/*    val job = SupervisorJob()
    val applicationScope = CoroutineScope(Dispatchers.Default + job)*/

    private var trueTimeDisposable: Disposable? = null

    override fun onCreate() {
        super.onCreate()
        initRxTrueTime()
        initKoin()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun stop() {
        trueTimeDisposable?.dispose()
    }

    private fun initKoin() {
        startKoin {
            androidLogger(Level.DEBUG)
            androidContext(this@SaherApp)
            modules(
                listOf(
                    repositoryModule,
                    viewModelModule,
                    retrofitModule,
                    apiModule
                )
            )
        }
    }

    private fun initRxTrueTime() {
        trueTimeDisposable = TrueTimeRx.build()
            .withConnectionTimeout(31428)
            .withRetryCount(100)
            .withSharedPreferencesCache(this)
            .initializeRx("time.google.com")
            .subscribeOn(Schedulers.io())
            .subscribe({ date: Date? ->
                Log.v(TAG, "TrueTime was initialized and we have a time: $date")
            }
            ) { throwable: Throwable ->
                throwable.message?.let { Log.w(TAG, it) }
            }

/*        applicationScope.launch {
            withContext(Dispatchers.IO) {
                try {
                    Log.e(TAG, "Initializing True Time")
                    TrueTime().withSharedPreferencesCache(this@SaherApp).build().initialize()
                } catch (e: IOException) {
                    Log.e(TAG, e.message.toString())
                }

            }
        }*/
    }

}