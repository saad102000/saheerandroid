package com.sa.saheer.ui.addAttendance

import android.annotation.SuppressLint
import android.app.TimePickerDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import com.sa.saheer.R
import com.sa.saheer.data.remote.models.LookupItem
import com.sa.saheer.databinding.FragmentAddAttendenceBinding
import com.sa.saheer.ui.base.BaseFragment
import com.sa.saheer.utils.IS_24_HOURS
import com.sa.saheer.utils.UI_TIME_FORMAT
import java.text.SimpleDateFormat
import java.util.*

class AddAttendanceFragment :
    BaseFragment<AddAttendanceViewModel, FragmentAddAttendenceBinding>(AddAttendanceViewModel::class) {

    private var selectedRegType: LookupItem? = null
    private var regTypesList: ArrayList<LookupItem>? = null
    private var selectedGaurd: LookupItem? = null
    private var supervisorGuards: ArrayList<LookupItem>? = null
    private var fromCal = Calendar.getInstance()

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentAddAttendenceBinding
        get() = FragmentAddAttendenceBinding::inflate

    override fun onResume() {
        super.onResume()
        askForLocationPermission()
    }

    override fun initObservers() {
        super.initObservers()

        viewModel.dateLiveData.observe(viewLifecycleOwner, Observer {
            if (it.isNullOrEmpty()) return@Observer
            binding.tvDate.text = it
        })

        viewModel.timeLiveData.observe(viewLifecycleOwner, Observer {
            if (it.isNullOrEmpty()) return@Observer
            binding.tvTime.text = it
        })

        viewModel.guardsLiveData.observe(viewLifecycleOwner, Observer {
            if (it == null) return@Observer
            onGuardsLoaded(it)
        })

        viewModel.regTypesLiveData.observe(viewLifecycleOwner, Observer {
            if (it == null) return@Observer
            onRegTypesLoaded(it)
        })

        viewModel.attendanceResult.observe(viewLifecycleOwner, {
            if (it) {
                onAddSuccess()
            }
        })

    }

    private fun onAddSuccess() {
        selectedRegType?.id?.let { typeId ->
            val msg = when (typeId) {
                1L -> R.string.check_in_success_msg
                2L -> R.string.check_out_success_msg
                else -> R.string.escape_success_msg
            }

            AlertDialog.Builder(requireContext())
                .setTitle(R.string.check_in_success_title)
                .setMessage(msg)
                .setPositiveButton(R.string.ok) { dialog, _ ->
                    dialog.dismiss()
                }
                .show()
        }
    }

    override fun initViews() {
        setTime()
        binding.etTime.setOnClickListener {
            showTimePicker()
        }

        binding.btnConfirm.setOnClickListener {
            checkIn()
        }
    }

    @SuppressLint("MissingPermission")
    private fun checkIn() {
        if (selectedRegType == null || selectedGaurd == null) {
            showError(R.string.error)
            return
        }

        selectedRegType?.id?.let { typeId ->
            if (latitude == null || longitude == null) {
                fusedLocationClient?.let {
                    it.lastLocation
                        .addOnSuccessListener { location ->
                            if (location != null) {
                                longitude = location.longitude
                                latitude = location.latitude
                                callCheckIn(typeId)
                            } else {
                                showError(R.string.no_location_available)
                            }

                        }
                }
            } else {
                callCheckIn(typeId)
            }
        }
    }

    private fun callCheckIn(typeId: Long) {
        when (typeId) {
            1L -> viewModel.checkIn(
                userId = selectedGaurd!!.id,
                latitude = latitude!!,
                longitude = longitude!!,
                time = fromCal.timeInMillis
            )

            2L -> viewModel.checkIn(
                false,
                userId = selectedGaurd!!.id,
                latitude = latitude!!,
                longitude = longitude!!,
                time = fromCal.timeInMillis
            )

            3L -> viewModel.absenceGuard(
                selectedGaurd!!.id,
                time = fromCal.timeInMillis,
                binding.etNotes.text.toString()
            )
        }
    }

    private fun onGuardsLoaded(guardsList: ArrayList<LookupItem>) {
        supervisorGuards = guardsList
        selectedGaurd = guardsList[0]
        val itemsArray = guardsList.map { it.name }
        val adapter =
            ArrayAdapter(
                requireContext(),
                R.layout.spinner_item,
                itemsArray
            )
        binding.guardsSpinner.adapter = adapter

        binding.guardsSpinner.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {

                }

                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    supervisorGuards?.let { list ->
                        selectedGaurd = list[position]
                    }
                }
            }
    }

    private fun onRegTypesLoaded(regTypes: ArrayList<LookupItem>) {
        regTypesList = regTypes
        selectedRegType = regTypes[0]
        val itemsArray = regTypes.map { it.name }
        val adapter =
            ArrayAdapter(
                requireContext(),
                R.layout.spinner_item,
                itemsArray
            )
        binding.registrationTypesSpinner.adapter = adapter

        binding.registrationTypesSpinner.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {

                }

                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    regTypesList?.let { list ->
                        selectedRegType = list[position]
                    }
                }
            }
    }

    private fun setTime() {
        val time = SimpleDateFormat(
            UI_TIME_FORMAT,
            Locale.getDefault()
        ).format(fromCal.timeInMillis)
        binding.etTime.text = time
    }

    private fun showTimePicker() {
        TimePickerDialog(
            requireContext(),
            { _, hourOfDay, minute ->
                fromCal.set(Calendar.HOUR_OF_DAY, hourOfDay)
                fromCal.set(Calendar.MINUTE, minute)
                setTime()
            },
            fromCal.get(Calendar.HOUR_OF_DAY),
            fromCal.get(Calendar.MINUTE),
            IS_24_HOURS
        ).show()
    }

}
