package com.sa.saheer.ui.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.sa.saheer.data.Repository
import com.sa.saheer.data.remote.Failure
import com.sa.saheer.utils.NetworkState
import kotlinx.coroutines.CoroutineExceptionHandler

abstract class BaseViewModel(private val repository: Repository) : ViewModel() {

    val _networkState = MutableLiveData<NetworkState>()
    val networkState: LiveData<NetworkState>
        get() = _networkState

    internal val handler = CoroutineExceptionHandler { _, ex ->
        when(ex){
            is Failure.UnauthorizedException ->{
                //todo show blocker dialog
                _networkState.postValue(NetworkState.error(ex.message ?: "Unknown Error"))
            }
            is Failure.UnknownException,
            is Failure.ServerException,
            is Failure.NotFoundException,
            is Failure.NetworkException ->{
                _networkState.postValue(NetworkState.error(ex.message ?: "Unknown Error"))
            }
            else ->{
                _networkState.value = NetworkState.error(ex.message ?:"Unknown Error")
            }
        }
    }
}