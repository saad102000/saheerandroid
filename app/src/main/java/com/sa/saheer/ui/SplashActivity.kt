package com.sa.saheer.ui

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import androidx.lifecycle.lifecycleScope
import com.sa.saheer.R
import com.sa.saheer.databinding.ActivitySplashBinding
import com.sa.saheer.ui.base.BaseActivity
import com.sa.saheer.ui.login.LoginActivity
import kotlinx.coroutines.delay
import splitties.activities.start

class SplashActivity : BaseActivity<ActivitySplashBinding>() {

    companion object {
        private const val SPLASH_TIME = 1000L
    }

    init {
        lifecycleScope.launchWhenCreated {
            delay(SPLASH_TIME)
            start<LoginActivity>()
            finish()
        }
    }


    override val bindingInflater: (LayoutInflater) -> ActivitySplashBinding
        get() = ActivitySplashBinding::inflate

    override fun initUi(savedInstanceState: Bundle?) {
        openFullScreen()
    }
}
