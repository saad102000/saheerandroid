package com.sa.saheer.ui.dailyReport

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.sa.saheer.data.Repository
import com.sa.saheer.data.remote.Result
import com.sa.saheer.data.remote.models.AttendanceItem
import com.sa.saheer.data.remote.models.LookupItem
import com.sa.saheer.ui.base.BaseViewModel
import com.sa.saheer.utils.Status
import com.sa.saheer.utils.getCurrentTimeMsServerFormat
import kotlinx.coroutines.launch
import java.util.*
import kotlin.collections.ArrayList

class DailyReportViewModel(private val repository: Repository) : BaseViewModel(repository) {

    private val _formStateLiveData = MutableLiveData<DailyReportFormState>()
    val formStateLiveData: LiveData<DailyReportFormState> get() = _formStateLiveData

    private val _guardsLiveData = MutableLiveData<ArrayList<LookupItem>?>()
    val guardsLiveData: LiveData<ArrayList<LookupItem>?> get() = _guardsLiveData

    private val _attendanceListLiveData = MutableLiveData<List<Pair<Long, List<AttendanceItem>>>?>()
    val attendanceList: LiveData<List<Pair<Long, List<AttendanceItem>>>?> get() = _attendanceListLiveData

    var loadGuardStatus: Status = Status.RUNNING
    var loadReportStatus: Status = Status.RUNNING

/*    init {
        getSupervisorGuards()
    }*/

    fun getSupervisorGuards() {
        viewModelScope.launch(handler) {
            _formStateLiveData.postValue(DailyReportFormState(loadGuardStatus = Status.RUNNING))
            when (val result = repository.getSupervisorGuards(repository.getUserId())) {
                is Result.Success -> {
                    _formStateLiveData.postValue(DailyReportFormState(loadGuardStatus = Status.SUCCESS))
                    result.data?.let {
                        val selfItem =
                            LookupItem(repository.getUserId(), repository.getUserName() ?: "انا")
                        val newList = ArrayList<LookupItem>()
                        newList.add(selfItem)
                        newList.addAll(it)
                        _guardsLiveData.postValue(newList)
                    }

                }
                is Result.Error -> {
                    _formStateLiveData.postValue(
                        DailyReportFormState(
                            loadGuardStatus = Status.RUNNING,
                            msg = result.exception.msg
                        )
                    )
                }
            }
        }
    }

    fun getAttendanceReport(from: Long, to: Long, userId: Long = repository.getUserId()) {
        viewModelScope.launch(handler) {
            if (from > to) {
                _formStateLiveData.postValue(DailyReportFormState(validDates = false))
                return@launch
            }

            _formStateLiveData.postValue(DailyReportFormState(loadReportStatus = Status.RUNNING))
            val fromDate = getCurrentTimeMsServerFormat(from)
            val toDate = getCurrentTimeMsServerFormat(to)
            when (val result = repository.attendanceReport(userId, fromDate, toDate)) {
                is Result.Success -> {
                    _formStateLiveData.postValue(DailyReportFormState(loadReportStatus = Status.SUCCESS))
                    result.data?.let {
                        _attendanceListLiveData.postValue(getGroupedItems(it))
                        return@launch
                    }

                    _attendanceListLiveData.postValue(null)
                }
                is Result.Error -> {
                    _formStateLiveData.postValue(
                        DailyReportFormState(
                            loadReportStatus = Status.RUNNING,
                            msg = result.exception.msg
                        )
                    )
                }
            }
        }
    }

    private fun getGroupedItems(list: ArrayList<AttendanceItem>): List<Pair<Long, List<AttendanceItem>>> {
        return list.groupBy {
            it.getAttendanceTimeCal().get(Calendar.DAY_OF_YEAR)
        }.map {
            Pair(it.value[0].getAttendanceTimeCal().timeInMillis, it.value)
        }
    }
}