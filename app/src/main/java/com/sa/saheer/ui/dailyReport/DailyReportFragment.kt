package com.sa.saheer.ui.dailyReport

import android.app.DatePickerDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.navigation.fragment.navArgs
import com.sa.saheer.R
import com.sa.saheer.data.remote.models.AttendanceItem
import com.sa.saheer.data.remote.models.LookupItem
import com.sa.saheer.databinding.FragmentDailyReportBinding
import com.sa.saheer.ui.base.BaseFragment
import com.sa.saheer.ui.registerAttendance.RegisterAttendanceFragmentArgs
import com.sa.saheer.utils.Status
import com.sa.saheer.utils.UI_DATE_FORMAT
import java.text.SimpleDateFormat
import java.util.*


class DailyReportFragment :
    BaseFragment<DailyReportViewModel, FragmentDailyReportBinding>(DailyReportViewModel::class) {

    private var selectedGaurd: LookupItem? = null
    private var supervisorGuards: ArrayList<LookupItem>? = null
    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentDailyReportBinding
        get() = FragmentDailyReportBinding::inflate

    private var fromCal = Calendar.getInstance()
    private var toCal = Calendar.getInstance()

    private var attendanceAdapter: GuardsAttendanceAdapter? = null

    private val navArgs: DailyReportFragmentArgs by navArgs()

    override fun initViews() {
        fromCal.apply {
            set(Calendar.HOUR_OF_DAY, getMinimum(Calendar.HOUR_OF_DAY))
            set(Calendar.MINUTE, getMinimum(Calendar.MINUTE))
        }

        toCal.apply {
            set(Calendar.HOUR_OF_DAY, getMaximum(Calendar.HOUR_OF_DAY))
            set(Calendar.MINUTE, getMaximum(Calendar.MINUTE))
        }
        setFromDate()
        setToDate()

        binding.tvFromDate.setOnClickListener {
            showFromDatePicker()
        }

        binding.tvToDate.setOnClickListener {
            showToDatePicker()
        }

        binding.btnConfirm.setOnClickListener {
            getReport()
        }

    }

    private fun getReport() {
        if(navArgs.isGaurd){
            viewModel.getAttendanceReport(
                fromCal.timeInMillis,
                toCal.timeInMillis
            )
        }
        selectedGaurd?.let { gaurd ->
            viewModel.getAttendanceReport(
                fromCal.timeInMillis,
                toCal.timeInMillis,
                gaurd.id,
            )
        }
    }

    override fun initObservers() {
        super.initObservers()

        viewModel.guardsLiveData.observe(viewLifecycleOwner, {
            if (it == null) return@observe
            onGuardsLoaded(it)
        })

        viewModel.formStateLiveData.observe(viewLifecycleOwner, {
            if (!it.validDates) {
                showError(R.string.to_date_is_before_from)
            } else if (it.loadGuardStatus == Status.RUNNING || it.loadReportStatus == Status.RUNNING) {
                showLoading()
            } else if (it.loadGuardStatus == Status.FAILED) {
                hideLoading()
                showStopperError(
                    R.string.error,
                    R.string.server_error,
                    { dialog, _ ->
                        dialog.dismiss()
                        viewModel.getSupervisorGuards()
                    }
                )
            } else if (it.loadReportStatus == Status.FAILED) {
                hideLoading()
                showStopperError(
                    R.string.error,
                    R.string.server_error,
                    { dialog, _ ->
                        dialog.dismiss()
                        getReport()
                    }
                )
            } else {
                hideLoading()
            }

        })

        viewModel.guardsLiveData.observe(viewLifecycleOwner, {
            if (it == null) return@observe
            onGuardsLoaded(it)
        })

        viewModel.attendanceList.observe(viewLifecycleOwner, {
            if (it == null) {
                binding.tvAttendanceMsg.visibility = View.VISIBLE
                binding.tvAttendanceMsg.text = getString(R.string.no_data)
                binding.rvGuards.visibility = View.GONE
                return@observe
            }
            onReportLoaded(it)
        })

        if(navArgs.isGaurd){
            binding.llGuards.visibility = View.GONE
            //binding.btnConfirm.performClick()
        }else{
            viewModel.getSupervisorGuards()
        }

    }

    private fun onReportLoaded(attendanceList: List<Pair<Long, List<AttendanceItem>>>) {
        binding.rvGuards.visibility = View.VISIBLE
        binding.tvAttendanceMsg.visibility = View.GONE
        attendanceAdapter = GuardsAttendanceAdapter(attendanceList)
        binding.rvGuards.adapter = attendanceAdapter
    }

    private fun setToDate() {
        val date = SimpleDateFormat(
            UI_DATE_FORMAT,
            Locale.getDefault()
        ).format(toCal.timeInMillis)
        binding.tvToDate.text = getString(R.string.date_to, date)
    }

    private fun setFromDate() {
        val date = SimpleDateFormat(
            UI_DATE_FORMAT,
            Locale.getDefault()
        ).format(fromCal.timeInMillis)
        binding.tvFromDate.text = getString(R.string.date_from, date)
    }


    private fun onGuardsLoaded(guardsList: ArrayList<LookupItem>) {
        supervisorGuards = guardsList
        selectedGaurd = guardsList[0]
        val itemsArray = guardsList.map { it.name }
        val adapter =
            ArrayAdapter(
                requireContext(),
                R.layout.spinner_item,
                itemsArray
            )
        binding.guardsSpinner.adapter = adapter

        binding.guardsSpinner.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {

                }

                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    supervisorGuards?.let { list ->
                        selectedGaurd = list[position]
                        getReport()
                    }
                }
            }
    }

    private fun showFromDatePicker() {
        val picker = DatePickerDialog(
            requireContext(),
            { _, year, monthOfYear, dayOfMonth ->
                fromCal.set(year, monthOfYear, dayOfMonth)
                setFromDate()
            },
            fromCal.get(Calendar.YEAR),
            fromCal.get(Calendar.MONTH),
            fromCal.get(Calendar.DAY_OF_MONTH)
        )
        picker.show()
    }

    private fun showToDatePicker() {
        val picker = DatePickerDialog(
            requireContext(),
            { _, year, monthOfYear, dayOfMonth ->
                toCal.set(year, monthOfYear, dayOfMonth)
                setToDate()
            },
            toCal.get(Calendar.YEAR),
            toCal.get(Calendar.MONTH),
            toCal.get(Calendar.DAY_OF_MONTH),
        )
        picker.datePicker.maxDate = Calendar.getInstance().timeInMillis
        picker.show()
    }


}
