package com.sa.saheer.ui.vacationRequest

import android.app.TimePickerDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AlertDialog
import androidx.navigation.fragment.NavHostFragment
import com.sa.saheer.R
import com.sa.saheer.data.remote.models.LookupItem
import com.sa.saheer.databinding.FragmentVacationRequestBinding
import com.sa.saheer.ui.base.BaseFragment
import com.sa.saheer.utils.IS_24_HOURS
import com.sa.saheer.utils.PERMISSION_ID
import com.sa.saheer.utils.UI_DATE_FORMAT
import com.sa.saheer.utils.UI_DATE_TIME_FORMAT
import java.text.SimpleDateFormat
import java.util.*

class VacationRequestFragment :
    BaseFragment<VacationRequestViewModel, FragmentVacationRequestBinding>(VacationRequestViewModel::class) {

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentVacationRequestBinding
        get() = FragmentVacationRequestBinding::inflate

    private var selectedLeaveType: LookupItem? = null
    private var leaveTypes: ArrayList<LookupItem>? = null
    private var fromCal = Calendar.getInstance()
    private var toCal = Calendar.getInstance()

    override fun initViews() {
        setFromDate()
        setToDate()
        binding.fromCalView.minDate = Calendar.getInstance().timeInMillis
        binding.toCalView.minDate = Calendar.getInstance().timeInMillis

        binding.fromCalView.setOnDateChangeListener { _, year, month, dayOfMonth ->
            fromCal.set(Calendar.YEAR, year)
            fromCal.set(Calendar.MONTH, month)
            fromCal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            if (isPermissionRequestMode()) {
                showFromTimePicker(false)
            } else {
                setFromDate()
            }
        }

        binding.toCalView.setOnDateChangeListener { _, year, month, dayOfMonth ->
            toCal.set(Calendar.YEAR, year)
            toCal.set(Calendar.MONTH, month)
            toCal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            if (isPermissionRequestMode()) {
                showToTimePicker()
            } else {
                setToDate()
            }
        }

        binding.btnConfirm.setOnClickListener {
            if (toCal.timeInMillis < fromCal.timeInMillis) {
                showError(R.string.to_date_is_before_from)
                return@setOnClickListener
            }

            val reason = binding.etReason.text.toString()

            /*if (reason.isEmpty()) {
                binding.etReason.error = getString(R.string.error_reason_empty)
                return@setOnClickListener
            }*/

            if (selectedLeaveType == null) {
                showError(R.string.error)
                return@setOnClickListener
            }
            viewModel.requestVacation(
                selectedLeaveType!!.id.toString(), fromCal.timeInMillis, toCal.timeInMillis, reason
            )
        }
    }

    override fun initObservers() {
        super.initObservers()

        viewModel.attendanceResult.observe(viewLifecycleOwner, {
            if (it) {
                onSubmitSuccess()
            }
        })

        viewModel.leaveTypes.observe(viewLifecycleOwner, { leavesList ->
            if (leavesList == null) {
                onFailedToLoadTypes()
            } else {
                onLoadLeaveTypes(leavesList)
            }
        })
    }

    private fun onLoadLeaveTypes(leavesList: ArrayList<LookupItem>) {
        leaveTypes = leavesList
        selectedLeaveType = leavesList[0]
        val itemsArray = leavesList.map { it.name }
        val adapter =
            ArrayAdapter(
                requireContext(),
                R.layout.spinner_item,
                itemsArray
            )
        binding.spinnerDropdown.adapter = adapter

        binding.spinnerDropdown.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {

                }

                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    leaveTypes?.let { list ->
                        selectedLeaveType = list[position]
                        setFromDate()
                        setToDate()
                        if (isPermissionRequestMode()) {
                            binding.llLeaveReason.visibility = View.VISIBLE
                            showFromTimePicker(true)
                        } else {
                            binding.llLeaveReason.visibility = View.GONE
                        }
                    }
                }
            }
    }

    private fun isPermissionRequestMode(): Boolean {
        return selectedLeaveType != null && selectedLeaveType?.id == PERMISSION_ID.toLong()
    }

    private fun showFromTimePicker(showToTimePicker: Boolean = false) {
        TimePickerDialog(
            requireContext(),
            { _, hourOfDay, minute ->
                fromCal.set(Calendar.HOUR_OF_DAY, hourOfDay)
                fromCal.set(Calendar.MINUTE, minute)
                setFromDate()
                if (showToTimePicker) showToTimePicker()
            },
            fromCal.get(Calendar.HOUR_OF_DAY),
            fromCal.get(Calendar.MINUTE),
            IS_24_HOURS
        ).show()
    }

    private fun showToTimePicker() {
        TimePickerDialog(
            requireContext(),
            { _, hourOfDay, minute ->
                toCal.set(Calendar.HOUR_OF_DAY, hourOfDay)
                toCal.set(Calendar.MINUTE, minute)
                setToDate()
            },
            toCal.get(Calendar.HOUR_OF_DAY),
            toCal.get(Calendar.MINUTE),
            IS_24_HOURS
        ).show()
    }

    private fun onFailedToLoadTypes() {
        AlertDialog.Builder(requireContext())
            .setTitle(R.string.vacation_request_load_types_failed_title)
            .setMessage(R.string.vacation_request_load_types_failed_msg)
            .setPositiveButton(R.string.ok) { dialog, _ ->
                dialog.dismiss()
                viewModel.getLeaveTypes()
            }
            .show()
    }

    private fun onSubmitSuccess() {
        AlertDialog.Builder(requireContext())
            .setTitle(R.string.leave_request_success_title)
            .setMessage(R.string.leave_request_success_msg)
            .setPositiveButton(R.string.ok) { dialog, _ ->
                dialog.dismiss()
                NavHostFragment.findNavController(this@VacationRequestFragment)
                    .navigateUp()
            }
            .show()
    }

    private fun setToDate() {
        val date = SimpleDateFormat(
            if (isPermissionRequestMode()) UI_DATE_TIME_FORMAT else UI_DATE_FORMAT,
            Locale.getDefault()
        ).format(toCal.timeInMillis)
        binding.tvDateTo.text = getString(R.string.date_to, date)
    }

    private fun setFromDate() {
        val date = SimpleDateFormat(
            if (isPermissionRequestMode()) UI_DATE_TIME_FORMAT else UI_DATE_FORMAT,
            Locale.getDefault()
        ).format(fromCal.timeInMillis)
        binding.tvDateFrom.text = getString(R.string.date_from, date)
    }

}
