package com.sa.saheer.ui.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.sa.saheer.R
import com.sa.saheer.databinding.FragmentGaurdHomeBinding
import com.sa.saheer.databinding.FragmentHomeBinding
import com.sa.saheer.ui.MainActivity
import com.sa.saheer.ui.base.BaseFragment

class HomeFragment : BaseFragment<HomeViewModel, FragmentGaurdHomeBinding>(HomeViewModel::class) {

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentGaurdHomeBinding
        get() = FragmentGaurdHomeBinding::inflate

    override fun initViews() {
        binding.llAttendance.setOnClickListener {
            it.findNavController().navigate(
                HomeFragmentDirections.actionHomeFragmentToRegisterAttendanceFragment2(true)
            )
        }

        binding.llLeaveRequest.setOnClickListener {
            it.findNavController()
                .navigate(HomeFragmentDirections.actionHomeFragmentToVacationRequestFragment())
        }

        binding.llCheckOut.setOnClickListener {
            val action =
                HomeFragmentDirections.actionHomeFragmentToRegisterAttendanceFragment2(false)
            it.findNavController().navigate(action)
        }

        binding.llDailyReport.setOnClickListener {
            val action =
                HomeFragmentDirections.actionHomeFragmentToDailyReportFragment2(true)
            it.findNavController().navigate(action)
        }

        findNavController().addOnDestinationChangedListener { controller, destination, arguments ->
            val title = when (destination.id) {
                R.id.registerAttendanceFragment2 -> {
                    if (arguments?.getBoolean("isCheckIn", true) == true) {
                        Pair(R.string.check_in, true)
                    } else {
                        Pair(R.string.check_out, true)
                    }
                }
                R.id.vacationRequestFragment -> Pair(R.string.leave_request, true)
                else -> Pair(R.string.app_title, false)
            }

            if (activity is MainActivity) {
                (activity as MainActivity).setTitleBar(title.first, title.second)
            }

        }

    }
}
