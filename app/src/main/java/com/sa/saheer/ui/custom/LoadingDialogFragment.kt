package com.sa.saheer.ui.custom

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.RelativeLayout
import androidx.fragment.app.DialogFragment
import com.sa.saheer.R


class LoadingDialogFragment : DialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.dialog_fragment_loading, container, false)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        // the content
        val root = RelativeLayout(activity).apply {
            layoutParams = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
        }

        // creating the fullscreen dialog
        val dialog: Dialog = object : Dialog(activity!!) {
            override fun onBackPressed() {
                if (isCancelable) {
                    dismiss()
                }
            }
        }

        dialog.apply {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            setContentView(root)
            setCanceledOnTouchOutside(false)
            val metrics = context.resources.displayMetrics
            val screenWidth = (metrics.widthPixels * 0.92).toInt()
            window?.apply {
                setLayout(screenWidth, ViewGroup.LayoutParams.WRAP_CONTENT)
                setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                //setBackgroundDrawable(context.getDrawable(R.drawable.rounded_corner_16dp))
                //setWindowAnimations(R.style.Dialog_Theme_Animation)
            }
        }
        return dialog
    }

}