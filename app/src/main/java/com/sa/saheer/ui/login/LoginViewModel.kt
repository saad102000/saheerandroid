package com.sa.saheer.ui.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.sa.saheer.R
import com.sa.saheer.data.Repository
import com.sa.saheer.data.remote.models.LoginResponse
import com.sa.saheer.utils.NetworkState
import kotlinx.coroutines.launch
import com.sa.saheer.ui.base.BaseViewModel

class LoginViewModel(val repository: Repository) : BaseViewModel(repository) {

    private val _userId = MutableLiveData<String?>()
    val userId: LiveData<String?> = _userId

    private val _userPw = MutableLiveData<String?>()
    val userPw: LiveData<String?> = _userPw

    private val _loginForm = MutableLiveData<LoginFormState>()
    val loginFormState: LiveData<LoginFormState> = _loginForm

    private val _loginResult = MutableLiveData<LoginResult>()
    val loginResult: LiveData<LoginResult> = _loginResult

    init {
        repository.getUserId().apply {
            if(this > -1) _userId.postValue(this.toString())
        }

        _userPw.postValue(repository.getPw())
    }

    fun login(username: String, password: String) {
        if(!loginDataChanged(username, password)) return
        _networkState.postValue(NetworkState.LOADING)
        viewModelScope.launch(handler) {
            val result = repository.login(username.toLong(), password)
            _networkState.postValue(NetworkState.LOADED)
            when (result) {
                is com.sa.saheer.data.remote.Result.Success -> {
                    _loginResult.value =
                        LoginResult(success = result.data?.let {
                            saveUserInfo(it, password)
                            LoggedInUserView(
                                displayName = it.fullname,
                                empRole = it.role
                            )
                        })
                }

                is com.sa.saheer.data.remote.Result.Error ->{
                    _networkState.postValue(NetworkState.error(result.exception.msg))
                    LoginResult(error = R.string.login_failed)
                }
            }
        }

    }

    private fun saveUserInfo(loginInfo: LoginResponse, password: String) {
        repository.setJwt(loginInfo.jwt)
        repository.setUserId(loginInfo.employeeId)
        repository.setUserRole(loginInfo.role)
        repository.setPw(password)
        repository.setUserName(loginInfo.fullname)
    }

    fun loginDataChanged(username: String, password: String):Boolean {
        return if (isUserNameValid(username) < 1) {
            _loginForm.value = LoginFormState(usernameError = R.string.invalid_username)
            false
        } else if (!isPasswordValid(password)) {
            _loginForm.value = LoginFormState(passwordError = R.string.invalid_password)
            false
        } else {
            _loginForm.value = LoginFormState(isDataValid = true)
            true
        }
    }

    // A placeholder username validation check
    private fun isUserNameValid(username: String?): Long {
        return if (username.isNullOrBlank()) return -1L
        else username.toLong()
    }

    // A placeholder password validation check
    private fun isPasswordValid(password: String?): Boolean {
        return if(password.isNullOrBlank()) false
        else password.length > 5
    }
}
