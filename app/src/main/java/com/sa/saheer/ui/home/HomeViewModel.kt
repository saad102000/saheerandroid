package com.sa.saheer.ui.home

import com.sa.saheer.data.Repository
import com.sa.saheer.ui.base.BaseViewModel

class HomeViewModel(val repository: Repository) : BaseViewModel(repository)  {
}