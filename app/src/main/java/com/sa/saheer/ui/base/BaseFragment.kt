package com.sa.saheer.ui.base

import android.content.DialogInterface
import android.location.LocationManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.viewbinding.ViewBinding
import com.google.android.gms.location.FusedLocationProviderClient
import com.sa.saheer.R
import com.sa.saheer.ui.custom.LoadingDialogFragment
import com.sa.saheer.utils.Status
import com.sa.saheer.utils.interfaces.OnItemClickListener
import com.sa.saheer.utils.toast
import org.koin.androidx.viewmodel.ext.android.viewModel
import kotlin.reflect.KClass

abstract class BaseFragment<out ViewModelType : BaseViewModel, VB : ViewBinding>(clazz: KClass<ViewModelType>) :
    Fragment() {

    companion object {
        const val POP_TAG = "POP_TAG"
    }

    open val viewModel: ViewModelType by viewModel(clazz = clazz)
    private val pop = LoadingDialogFragment()

    var longitude: Double? = null
        get() {
            if (activity is BaseActivity<*>) {
                (activity as BaseActivity<*>).apply {
                    return longitude
                }
            }

            return null
        }
    var latitude: Double? = null
        get() {
            if (activity is BaseActivity<*>) {
                (activity as BaseActivity<*>).apply {
                    return latitude
                }
            }

            return null
        }

    val locationManager: LocationManager?
        get() {
            if (activity is BaseActivity<*>) {
                (activity as BaseActivity<*>).apply {
                    return locationManager
                }
            }

            return null
        }

    val fusedLocationClient: FusedLocationProviderClient?
        get() {
            if (activity is BaseActivity<*>) {
                (activity as BaseActivity<*>).apply {
                    return fusedLocationClient
                }
            }

            return null
        }

    protected var _binding: VB? = null
    abstract val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> VB
    protected val binding: VB
        get() = _binding as VB

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = bindingInflater.invoke(inflater, container, false)
        return requireNotNull(_binding).root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        hideLoading()
        _binding = null
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initViews()
        initObservers()
    }

    fun initTitle(@StringRes resId: Int, isBackEnabled: Boolean = false) {
        initTitle(getString(resId), isBackEnabled)
    }

    fun initTitle(title: String, isBackEnabled: Boolean = false) {
        (activity as AppCompatActivity).apply {
            supportActionBar?.apply {
                setDisplayHomeAsUpEnabled(isBackEnabled)
                setTitle(title)
            }
        }
    }

    abstract fun initViews()

    open fun initObservers() {
        viewModel.networkState.observe(viewLifecycleOwner, Observer {
            val state = it ?: return@Observer
            when (state.status) {
                Status.RUNNING -> showLoading()
                Status.SUCCESS -> hideLoading()
                Status.FAILED -> {
                    hideLoading()
                    state.msg?.let { it1 -> showError(it1) }
                }
            }
        })
    }

    fun showLoading() {
        pop.show(parentFragmentManager, POP_TAG)
    }

    fun hideLoading() {
        if (pop.isAdded)
            pop.dismiss()
    }

    fun showError(@StringRes errorString: Int) {
        showError(getString(errorString))
    }

    fun showError(errorString: String) {
        context?.toast(errorString)
    }

    fun showStopperError(
        title: String,
        errorString: String,
        listener: DialogInterface.OnClickListener? = null,
        cancellable: Boolean = true
    ) {
        AlertDialog.Builder(requireContext())
            .setTitle(title)
            .setMessage(errorString)
            .setPositiveButton(R.string.ok, listener)
            .setCancelable(cancellable)
            .show()
    }

    fun showStopperError(
        @StringRes title: Int,
        @StringRes errorString: Int,
        listener: DialogInterface.OnClickListener? = null,
        cancellable: Boolean = true
    ) {
        showStopperError(getString(title), getString(errorString), listener, cancellable)
    }

    fun showMessage(@StringRes msgString: Int) {
        showError(getString(msgString))
    }

    fun showMessage(msgString: String) {
        context?.toast(msgString)
    }

    fun askForLocationPermission(onItemClickListener: OnItemClickListener<Boolean>? = null) {
        if (activity is BaseActivity<*>) {
            (activity as BaseActivity<*>).apply {
                askForLocationPermission(onItemClickListener)
            }
        }
    }

    fun getLocation() {
        if (activity is BaseActivity<*>) {
            (activity as BaseActivity<*>).apply {
                //getLocation()
                getLastKnownLocation()
            }
        }
    }
}
