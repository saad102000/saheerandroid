package com.sa.saheer.ui.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.sa.saheer.R
import com.sa.saheer.databinding.FragmentSupervisorHomeBinding
import com.sa.saheer.ui.MainActivity
import com.sa.saheer.ui.base.BaseFragment

class SupervisorHomeFragment :
    BaseFragment<HomeViewModel, FragmentSupervisorHomeBinding>(HomeViewModel::class) {

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentSupervisorHomeBinding
        get() = FragmentSupervisorHomeBinding::inflate

    override fun initViews() {
        binding.llAttendance.setOnClickListener {
            it.findNavController().navigate(
                SupervisorHomeFragmentDirections.actionSupervisorHomeFragmentToRegisterAttendanceFragment(
                    true
                )
            )
        }

        binding.llLeaveRequest.setOnClickListener {
            it.findNavController()
                .navigate(SupervisorHomeFragmentDirections.actionSupervisorHomeFragmentToVacationRequestFragment())
        }

        binding.llAddAttendance.setOnClickListener {
            it.findNavController()
                .navigate(SupervisorHomeFragmentDirections.actionSupervisorHomeFragmentToAddAttendanceFragment())
        }

        binding.llDailyReport.setOnClickListener {
            it.findNavController()
                .navigate(SupervisorHomeFragmentDirections.actionSupervisorHomeFragmentToDailyReportFragment())
        }

        binding.llCheckOut.setOnClickListener {
            it.findNavController().navigate(
                SupervisorHomeFragmentDirections.actionSupervisorHomeFragmentToRegisterAttendanceFragment(
                    false
                )
            )
        }

        findNavController().addOnDestinationChangedListener { _, destination, arguments ->
            val title = when (destination.id) {
                R.id.registerAttendanceFragment -> {
                    if (arguments?.getBoolean("isCheckIn", true) == true) {
                        Pair(R.string.check_in, true)
                    } else {
                        Pair(R.string.check_out, true)
                    }
                }
                R.id.vacationRequestFragment -> Pair(R.string.leave_request, true)
                R.id.dailyReportFragment -> Pair(R.string.daily_report, true)
                R.id.addAttendanceFragment -> Pair(R.string.add_attendance, true)
                else -> Pair(R.string.app_title, false)
            }

            if (activity is MainActivity) {
                (activity as MainActivity).setTitleBar(title.first, title.second)
            }

        }
    }


}
