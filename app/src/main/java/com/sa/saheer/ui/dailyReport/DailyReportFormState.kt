package com.sa.saheer.ui.dailyReport

import com.sa.saheer.utils.Status

/**
 * Data validation state of the login form.
 */
data class DailyReportFormState(
    val loadGuardStatus: Status = Status.SUCCESS,
    val loadReportStatus: Status = Status.SUCCESS,
    val validDates: Boolean = true,
    val msg: String? = null
)
