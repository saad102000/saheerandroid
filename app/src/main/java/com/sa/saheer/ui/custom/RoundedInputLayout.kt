package com.sa.saheer.ui.custom

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.inputmethod.EditorInfo
import androidx.appcompat.widget.LinearLayoutCompat
import com.sa.saheer.R
import com.sa.saheer.databinding.RoundedInputBinding

class RoundedInputLayout : LinearLayoutCompat {

    private var binding: RoundedInputBinding =
        RoundedInputBinding.inflate(LayoutInflater.from(context), this, true)

    var text: String
        get() = binding.etInput.text.toString().trim()
        set(value) {
            binding.etInput.setText(value)
        }

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        initialize()

        //val attributes = context.obtainStyledAttributes(attrs, R.styleable.RoundedInputLayout)
        attrs?.apply {

            val attributes = context.obtainStyledAttributes(
                this,
                R.styleable.RoundedInputLayout,
                defStyleAttr,
                0
            )
            attributes.apply {
                binding.tvLabel.text = getString(R.styleable.RoundedInputLayout_label)
                binding.etInput.apply {
                    setText(getString(R.styleable.RoundedInputLayout_inputText))
                    imeOptions = EditorInfo.IME_ACTION_DONE
                }
                recycle()

            }
        }
    }

    private fun initialize() {

    }

}