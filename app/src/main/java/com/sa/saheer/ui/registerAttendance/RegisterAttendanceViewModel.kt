package com.sa.saheer.ui.registerAttendance

import android.text.format.DateUtils
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.sa.saheer.data.Repository
import com.sa.saheer.data.remote.Result
import com.sa.saheer.data.remote.models.AttendanceItem
import com.sa.saheer.data.remote.models.AttendanceRequest
import com.sa.saheer.utils.*
import kotlinx.coroutines.launch
import com.sa.saheer.ui.base.BaseViewModel
import java.text.SimpleDateFormat
import java.util.*

class RegisterAttendanceViewModel(val repository: Repository) : BaseViewModel(repository) {

    private val _lastCheckTimeLiveData = MutableLiveData<Pair<Boolean, String>?>()
    val lastCheckTimeLiveData: LiveData<Pair<Boolean, String>?> get() = _lastCheckTimeLiveData

    private val _idLiveData = MutableLiveData<String>()
    val idLiveData: LiveData<String> get() = _idLiveData

    private val _dateLiveData = MutableLiveData<String>()
    val dateLiveData: LiveData<String> get() = _dateLiveData

    private val _timeLiveData = MutableLiveData<String>()
    val timeLiveData: LiveData<String> get() = _timeLiveData

    private val _attendanceResult = MutableLiveData<Boolean>()
    val attendanceResult: LiveData<Boolean> get() = _attendanceResult

    init {
        getAttendanceReport()
        val timeMs = System.currentTimeMillis()

        val date = SimpleDateFormat(UI_DATE_FORMAT, Locale.getDefault()).format(timeMs)
        val time = SimpleDateFormat(UI_TIME_FORMAT, Locale.getDefault()).format(timeMs)

        _dateLiveData.postValue(date)
        _timeLiveData.postValue(time)
        _idLiveData.postValue(repository.getUserId().toString())
    }

    fun registerAttendance(
        isCheckIn: Boolean = true,
        faceImage: String = DEVICE_ID,
        latitude: Double,
        longitude: Double
    ) {
        _networkState.postValue(NetworkState.LOADING)
        viewModelScope.launch(handler) {
            val attendanceTime = getCurrentTimeMsServerFormat()
            val attendanceReq = AttendanceRequest(attendanceTime, faceImage, latitude, longitude)

            val result = if (isCheckIn)
                repository.attendanceCheckIn(repository.getUserId(), attendanceReq)
            else repository.attendanceCheckOut(repository.getUserId(), attendanceReq)

            when (result) {
                is Result.Success -> {
                    _networkState.postValue(NetworkState.LOADED)
                    _attendanceResult.postValue(true)
                }
                is Result.Error -> {
                    _networkState.postValue(NetworkState.error(result.exception.msg))
                    _attendanceResult.postValue(false)
                }
            }
        }
    }


    fun getAttendanceReport() {
        viewModelScope.launch(handler) {
            val result = repository.attendanceReport(
                repository.getUserId(),
                getCurrentTimeMsServerFormat(Calendar.getInstance().timeInMillis - 2 * DateUtils.DAY_IN_MILLIS),
                getCurrentTimeMsServerFormat(Calendar.getInstance().timeInMillis)
            )

            when (result) {
                is Result.Success -> {
                    getLastCheckDate(result.data)
                }
                is Result.Error -> {
                    _lastCheckTimeLiveData.postValue(null)
                }
            }
        }
    }

    private fun getLastCheckDate(list: ArrayList<AttendanceItem>?) {
        list?.map {
            Pair(
                SimpleDateFormat(SERVER_DATE_TIME_FORMAT, Locale("ar")).parse(it.attendanceTime),
                it.isCheckIn
            )
        }?.maxByOrNull { it.first }?.let { maxPair ->
            val date =
                SimpleDateFormat(UI_DATE_TIME_FORMAT, Locale.getDefault()).format(maxPair.first?.time)
            _lastCheckTimeLiveData.postValue(Pair(maxPair.second, date))
            return
        }

        _lastCheckTimeLiveData.postValue(null)
    }


}