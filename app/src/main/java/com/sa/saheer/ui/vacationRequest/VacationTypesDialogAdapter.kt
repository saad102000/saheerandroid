package com.sa.saheer.ui.vacationRequest

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.sa.saheer.data.remote.models.LookupItem
import com.sa.saheer.databinding.ItemLeaveTypeBinding
import com.sa.saheer.utils.interfaces.OnItemClickListener

class VacationTypesDialogAdapter(
    val listener: OnItemClickListener<LookupItem>,
    val mValues: List<LookupItem>?
) : RecyclerView.Adapter<VacationTypesDialogAdapter.TypeViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TypeViewHolder {
        val binding = ItemLeaveTypeBinding
            .inflate(LayoutInflater.from(parent.context), parent, false)
        return TypeViewHolder(binding)
    }

    override fun onBindViewHolder(holder: TypeViewHolder, position: Int) {
        holder.bind(position)
    }

    override fun getItemCount(): Int = mValues?.size ?: 0

    inner class TypeViewHolder(val binding: ItemLeaveTypeBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(position: Int) {
            mValues?.let {
                binding.tvText.text = it[position].name

                binding.root.setOnClickListener { listener.onItemClicked(mValues[position]) }
            }
        }
    }
}