package com.sa.saheer.ui.addAttendance

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.sa.saheer.data.Repository
import com.sa.saheer.data.remote.Result
import com.sa.saheer.data.remote.models.AbsenceRequest
import com.sa.saheer.data.remote.models.AttendanceRequest
import com.sa.saheer.data.remote.models.LookupItem
import com.sa.saheer.ui.base.BaseViewModel
import com.sa.saheer.utils.*
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*

class AddAttendanceViewModel(private val repository: Repository) : BaseViewModel(repository) {

    private val _dateLiveData = MutableLiveData<String>()
    val dateLiveData: LiveData<String> get() = _dateLiveData

    private val _timeLiveData = MutableLiveData<String>()
    val timeLiveData: LiveData<String> get() = _timeLiveData

    private val _guardsLiveData = MutableLiveData<ArrayList<LookupItem>?>()
    val guardsLiveData: LiveData<ArrayList<LookupItem>?> get() = _guardsLiveData

    private val _regTypesLiveData = MutableLiveData<ArrayList<LookupItem>?>()
    val regTypesLiveData: LiveData<ArrayList<LookupItem>?> get() = _regTypesLiveData

    private val _attendanceResult = MutableLiveData<Boolean>()
    val attendanceResult: LiveData<Boolean> get() = _attendanceResult

    init {
        val timeMs = System.currentTimeMillis()

        val date = SimpleDateFormat(UI_DATE_FORMAT, Locale.getDefault()).format(timeMs)
        val time = SimpleDateFormat(UI_TIME_FORMAT, Locale.getDefault()).format(timeMs)

        _dateLiveData.postValue(date)
        _timeLiveData.postValue(time)

        _regTypesLiveData.postValue(
            arrayListOf(
                LookupItem(1, "تسجيل حضور"),
                LookupItem(2, "تسجيل إنصراف"),
                LookupItem(2, "تسجيل إنسحاب")
            )
        )

        getSupervisorGuards()
    }

    private fun getSupervisorGuards() {
        viewModelScope.launch(handler) {
            _networkState.postValue(NetworkState.LOADING)
            when (val result = repository.getSupervisorGuards(repository.getUserId())) {
                is Result.Success -> {
                    _networkState.postValue(NetworkState.LOADED)
                    result.data?.let {
                        _guardsLiveData.postValue(it)
                    }

                }
                is Result.Error -> {
                    _networkState.postValue(NetworkState.error(result.exception.msg))
                    _guardsLiveData.postValue(null)
                }
            }
        }
    }


    fun checkIn(
        isCheckIn: Boolean = true,
        userId: Long,
        faceImage: String = DEVICE_ID,
        latitude: Double,
        longitude: Double,
        time: Long
    ) {
        viewModelScope.launch(handler) {
            _networkState.postValue(NetworkState.LOADING)
            val attendanceTime = getCurrentTimeMsServerFormat(time)
            val attendanceReq = AttendanceRequest(attendanceTime, faceImage, latitude, longitude)

            val result = if (isCheckIn) repository.attendanceCheckIn(userId, attendanceReq)
            else repository.attendanceCheckOut(userId, attendanceReq)

            when (result) {
                is Result.Success -> {
                    _networkState.postValue(NetworkState.LOADED)
                    _attendanceResult.postValue(true)
                }
                is Result.Error -> {
                    _networkState.postValue(NetworkState.error(result.exception.msg))
                    _attendanceResult.postValue(false)
                }
            }
        }
    }

    fun absenceGuard(guardId: Long, time: Long, notes: String) {
        viewModelScope.launch(handler) {
            _networkState.postValue(NetworkState.LOADING)
            val absenceTime = getCurrentTimeMsServerFormat(time)
            val attendanceReq = AbsenceRequest(absenceTime, guardId, notes)
            when (val result = repository.absence(attendanceReq)) {
                is Result.Success -> {
                    _networkState.postValue(NetworkState.LOADED)
                    _attendanceResult.postValue(true)
                }
                is Result.Error -> {
                    _networkState.postValue(NetworkState.error(result.exception.msg))
                    _attendanceResult.postValue(false)
                }
            }
        }
    }

}