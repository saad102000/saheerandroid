package com.sa.saheer.ui.custom

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.LinearLayoutCompat
import com.google.android.material.textview.MaterialTextView
import com.sa.saheer.R
import com.sa.saheer.databinding.RoundedInputBinding

class RoundedSpinnerLayout(context: Context, attrs: AttributeSet) :
    LinearLayoutCompat(context, attrs) {

    //private var binding: RoundedInputBinding

    init {
        inflate(context, R.layout.rounded_input, this)

        val tvLabel: MaterialTextView = findViewById(R.id.tvLabel)
        val etInput: AppCompatEditText = findViewById(R.id.etInput)
       /* LayoutInflater.from(context)
            .inflate(R.layout.rounded_input, this, true)

        binding = RoundedInputBinding.inflate(LayoutInflater.from(context), this, true)
*/
        val attributes = context.obtainStyledAttributes(attrs, R.styleable.RoundedInputLayout)
        tvLabel.text = attributes.getString(R.styleable.RoundedInputLayout_label)
        etInput.setText(attributes.getString(R.styleable.RoundedInputLayout_inputText))
        attributes.recycle()
    }
}