package com.sa.saheer.ui.dailyReport

import android.text.method.ScrollingMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.sa.saheer.data.remote.models.AttendanceItem
import com.sa.saheer.databinding.ItemAttendanceBinding
import com.sa.saheer.utils.interfaces.OnItemClickListener
import kotlin.properties.Delegates

class AttendanceAdapter(private var mValues: List<AttendanceItem>) : RecyclerView.Adapter<AttendanceAdapter.ItemViewHolder>() {

/*
    private var mValues: List<AttendanceItem> by Delegates.observable(emptyList()) { _, _, _ ->
        this.notifyDataSetChanged()
    }
*/

    fun updateList(newList: List<AttendanceItem>) {
        this.mValues = newList
        notifyDataSetChanged()
    }

    var listener: OnItemClickListener<AttendanceItem>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val binding = ItemAttendanceBinding
            .inflate(LayoutInflater.from(parent.context), parent, false)
        return ItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        //holder.bind(position)
    }

    override fun getItemCount(): Int = mValues.size
    inner class ItemViewHolder(val binding: ItemAttendanceBinding) :
        RecyclerView.ViewHolder(binding.root) {

/*        fun bind(position: Int) {
            binding.run {
               tvName.text = mValues[position].attendanceTime
               tvId.text = mValues[position].id.toString()
               tvAttendanceTime.text = mValues[position].attendanceTime
               tvLeaveTime.text = mValues[position].isCheckIn.toString()

                root.setOnClickListener { listener?.onItemClicked(mValues[position]) }
            }
        }*/
    }

/*
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val inflate =
            ItemAttendanceBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ItemViewHolder(inflate.root)
    }

    inner class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val binding = ItemAttendanceBinding.bind(view)

        fun bind(position: Int) {
            binding.run {
                tvName.text = mValues[position].attendanceTime
                tvId.text = mValues[position].id.toString()
                tvAttendanceTime.text = mValues[position].attendanceTime
                tvLeaveTime.text = mValues[position].isCheckIn.toString()

                root.setOnClickListener { listener?.onItemClicked(mValues[position]) }
            }
        }
    }*/
}