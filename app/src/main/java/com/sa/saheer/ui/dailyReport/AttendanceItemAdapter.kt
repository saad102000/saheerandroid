package com.sa.saheer.ui.dailyReport

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.sa.saheer.R
import com.sa.saheer.data.remote.models.AttendanceItem
import com.sa.saheer.utils.UI_TIME_FORMAT
import com.sa.saheer.utils.interfaces.OnItemClickListener
import java.text.SimpleDateFormat
import java.util.*

class AttendanceItemAdapter(
    private val values: List<AttendanceItem>
) : RecyclerView.Adapter<AttendanceItemAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.check_item, parent, false)
        return ViewHolder(view)
    }

    var listener: OnItemClickListener<AttendanceItem>? = null

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        holder.apply {

            if (item.isCheckIn){
                tvTitle.text = itemView.context.getString(R.string.check_in)
                mainLayout.setBackgroundColor(ContextCompat.getColor(itemView.context,R.color.colorGreen))
            }else{
                tvTitle.text = itemView.context.getString(R.string.check_out)
                mainLayout.setBackgroundColor(ContextCompat.getColor(itemView.context,R.color.colorLightRed))
            }

            tvAttendanceTime.text = SimpleDateFormat(
                UI_TIME_FORMAT,
                Locale.getDefault()
            ).format(item.getAttendanceTimeCal().time)
        }
    }

    override fun getItemCount(): Int = values.size

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tvTitle: TextView = view.findViewById(R.id.tvTitle)
        val tvAttendanceTime: TextView = view.findViewById(R.id.tvAttendanceTime)
        val mainLayout: ViewGroup = view.findViewById(R.id.mainLayout)
    }
}