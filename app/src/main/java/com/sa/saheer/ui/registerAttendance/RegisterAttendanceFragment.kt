package com.sa.saheer.ui.registerAttendance

import android.annotation.SuppressLint
import android.hardware.fingerprint.FingerprintManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.navArgs
import com.sa.saheer.R
import com.sa.saheer.databinding.FragmentRegisterAttendanceBinding
import com.sa.saheer.ui.base.BaseFragment
import de.adorsys.android.finger.Finger
import de.adorsys.android.finger.FingerListener


class RegisterAttendanceFragment :
    BaseFragment<RegisterAttendanceViewModel, FragmentRegisterAttendanceBinding>(
        RegisterAttendanceViewModel::class
    ), FingerListener {

    private var userId: String? = null
    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentRegisterAttendanceBinding
        get() = FragmentRegisterAttendanceBinding::inflate

    private val args: RegisterAttendanceFragmentArgs by navArgs()

    private lateinit var finger: Finger

    override fun initObservers() {
        super.initObservers()
        viewModel.idLiveData.observe(viewLifecycleOwner, Observer {
            if (it.isNullOrEmpty()) return@Observer
            userId = it
        })

        viewModel.dateLiveData.observe(viewLifecycleOwner, Observer {
            if (it.isNullOrEmpty()) return@Observer
            binding.tvDate.text = it
        })

        viewModel.timeLiveData.observe(viewLifecycleOwner, Observer {
            if (it.isNullOrEmpty()) return@Observer
            binding.tvTime.text = it
        })

        viewModel.attendanceResult.observe(viewLifecycleOwner, {
            if (it) {
                showStopperError(R.string.check_in_success_title,
                    if (args.isCheckIn) R.string.check_in_success_msg else R.string.check_out_success_msg,
                    { dialog, which ->
                        dialog.dismiss()
                        NavHostFragment.findNavController(this@RegisterAttendanceFragment)
                            .navigateUp()
                    })
            }
        })

        viewModel.lastCheckTimeLiveData.observe(viewLifecycleOwner, Observer {
            if (it == null) {
                binding.tvLastCheckTime.visibility = View.INVISIBLE
                return@Observer
            }
            binding.tvLastCheckTime.visibility = View.VISIBLE
            val attendance = if (it.first) R.string.check_in else R.string.check_out
            binding.tvLastCheckTime.text =
                getString(R.string.last_check_in, getString(attendance), it.second)
        })
    }

    override fun onResume() {
        super.onResume()
        askForLocationPermission()
    }

    override fun initViews() {
        //initTitle(R.string.attendance_submit, true)
        finger = Finger(
            requireContext(), mapOf(
                Pair(
                    FingerprintManager.FINGERPRINT_ERROR_HW_UNAVAILABLE,
                    getString(R.string.error_override_hw_unavailable)
                )
            )
        )

        finger.subscribe(this)

        val fingerprintsEnabled = finger.hasFingerprintEnrolled()

        binding.ivFingerPrint.setImageResource(if (fingerprintsEnabled) R.drawable.ic_fingerprint_on else R.drawable.ic_fingerprint_off)

        binding.btnTouchFingerPrint.isEnabled = fingerprintsEnabled

        binding.btnTouchFingerPrint.setOnClickListener {
            showPermissionDialog()
        }

        if (!fingerprintsEnabled) {
            showError(R.string.error_override_hw_unavailable)
        }

        binding.btnSignIn.isEnabled = !fingerprintsEnabled
        binding.btnSignIn.setOnClickListener {
            if (binding.etId.text != userId) {
                showError(R.string.invalid_user_id)
                checkIn()
            }
        }
    }

    private fun showPermissionDialog() {
        finger.showDialog(
            requireActivity(),
            Finger.DialogStrings(title = getString(R.string.text_fingerprint))
        )
    }

    override fun onFingerprintAuthenticationFailure(errorMessage: String, errorCode: Int) {
        showError(errorMessage)
        binding.ivFingerPrint.setImageResource(R.drawable.ic_fingerprint_off)
        finger.subscribe(this)
    }


    override fun onFingerprintAuthenticationSuccess() {
        showMessage(R.string.message_success)
        binding.ivFingerPrint.setImageResource(R.drawable.ic_fingerprint_on)
        finger.subscribe(this)
        checkIn()
    }

    @SuppressLint("MissingPermission")
    private fun checkIn() {
        if (latitude == null || longitude == null) {
            fusedLocationClient?.let {
                it.lastLocation
                    .addOnSuccessListener { location ->
                        if (location != null) {
                            longitude = location.longitude
                            latitude = location.latitude

                            viewModel.registerAttendance(
                                args.isCheckIn,
                                latitude = latitude!!,
                                longitude = longitude!!
                            )
                        } else {
                            showError(R.string.no_location_available)
                        }
                    }
            }
        } else {
            viewModel.registerAttendance(
                args.isCheckIn,
                latitude = latitude!!,
                longitude = longitude!!
            )
        }
    }

}
