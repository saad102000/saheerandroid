package com.sa.saheer.ui.base

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.content.res.Resources
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.WindowInsets
import android.view.WindowManager
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.viewbinding.ViewBinding
import com.akexorcist.localizationactivity.core.LocalizationActivityDelegate
import com.akexorcist.localizationactivity.core.OnLocaleChangedListener
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.permissionx.guolindev.PermissionX
import com.sa.saheer.R
import com.sa.saheer.utils.PermissionsHelper
import com.sa.saheer.utils.interfaces.OnItemClickListener
import java.util.*

abstract class BaseActivity<VB : ViewBinding> : AppCompatActivity(), OnLocaleChangedListener {
    private val localizationDelegate: LocalizationActivityDelegate by lazy {
        LocalizationActivityDelegate(this)
    }
    //abstract fun initToolBar(isBackEnabled: Boolean = false)

    var longitude: Double? = null
    var latitude: Double? = null
    var locationManager: LocationManager? = null

    var fusedLocationClient: FusedLocationProviderClient? = null

    private var _binding: ViewBinding? = null
    abstract val bindingInflater: (LayoutInflater) -> VB
    @Suppress("UNCHECKED_CAST")
    protected val binding: VB
        get() = _binding as VB

    override fun onCreate(savedInstanceState: Bundle?) {
        localizationDelegate.addOnLocaleChangedListener(this)
        localizationDelegate.onCreate()
        super.onCreate(savedInstanceState)
        _binding = bindingInflater.invoke(layoutInflater)
        setContentView(binding.root)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        initUi(savedInstanceState)
        initObservers()
    }

    open fun initObservers() {

    }

    abstract fun initUi(savedInstanceState: Bundle?)

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onResume() {
        super.onResume()
        localizationDelegate.onResume(this)
    }

    override fun attachBaseContext(newBase: Context) {
        applyOverrideConfiguration(localizationDelegate.updateConfigurationLocale(newBase))
        super.attachBaseContext(newBase)
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        localizationDelegate.getApplicationContext(this)
    }

    override fun getBaseContext(): Context {
        return localizationDelegate.getApplicationContext(super.getBaseContext())
    }

    override fun getApplicationContext(): Context {
        return localizationDelegate.getApplicationContext(super.getApplicationContext())
    }

    override fun getResources(): Resources {
        return localizationDelegate.getResources(super.getResources())
    }

    fun setLanguage(language: String?) {
        localizationDelegate.setLanguage(this, language!!)
    }

    fun setLanguage(locale: Locale?) {
        localizationDelegate.setLanguage(this, locale!!)
    }

    fun getCurrentLanguage(): Locale = localizationDelegate.getLanguage(applicationContext)

    // Just override method locale change event
    override fun onBeforeLocaleChanged() {}
    override fun onAfterLocaleChanged() {}

    @Suppress("DEPRECATION")
    fun openFullScreen() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            window.insetsController?.hide(WindowInsets.Type.statusBars())
        } else {
            window.setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN
            )
        }
    }


    fun askForLocationPermission(onItemClickListener: OnItemClickListener<Boolean>? = null) {
        PermissionX.init(this)
            .permissions(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
            )
            .explainReasonBeforeRequest()
            .onExplainRequestReason { scope, deniedList ->
                scope.showRequestReasonDialog(
                    deniedList,
                    getString(R.string.permission_fine_location_message),
                    getString(R.string.ok),
                    getString(R.string.cancel)
                )
            }
            .onForwardToSettings { scope, deniedList ->
                scope.showForwardToSettingsDialog(
                    deniedList,
                    getString(R.string.gps_enable_msg),
                    getString(android.R.string.ok)
                )
            }
            .request { allGranted, grantedList, deniedList ->
                if (allGranted) {
                    //showMessage(R.string.permission_fine_location_granted)
                    //getLocation()
                    getLastKnownLocation()
                }
                /*else {
                     NavHostFragment.findNavController(this).navigateUp()
                }*/

                onItemClickListener?.onItemClicked(allGranted)
            }
    }

/*
    fun getLocation() {
        if (ActivityCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) return
        locationManager =
            this.getSystemService(Context.LOCATION_SERVICE) as LocationManager

        locationManager?.let {
            val isGPSEnabled = it.isProviderEnabled(LocationManager.GPS_PROVIDER)
            val isNetworkEnabled = it.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
            if (!isGPSEnabled || !isNetworkEnabled) {
                PermissionsHelper.showSettingsAlert(this)
            } else {
                val provider =
                    if (isGPSEnabled) LocationManager.GPS_PROVIDER else LocationManager.NETWORK_PROVIDER
                try {
                    it.requestLocationUpdates(provider, 20 * 5000, 25f, this)
                } catch (ex: SecurityException) {
                    Log.d("Location", "Security Exception, no location available")
                }
            }
        }
    }
*/

    @SuppressLint("MissingPermission")
    fun getLastKnownLocation() {
        if (!isLocationPermissionsGranted()) {
            askForLocationPermission()
            return
        }
        locationManager =
            this.getSystemService(Context.LOCATION_SERVICE) as LocationManager

        locationManager?.let {
            val isGPSEnabled = it.isProviderEnabled(LocationManager.GPS_PROVIDER)
            val isNetworkEnabled = it.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
            if (!isGPSEnabled || !isNetworkEnabled) {
                PermissionsHelper.showSettingsAlert(this)
                return
            }
            fusedLocationClient?.let {
                it.lastLocation
                    .addOnSuccessListener { location ->
                        if (location != null) {
                            longitude = location.longitude
                            latitude = location.latitude
                        }

                    }
            }

        }
    }

    /*override fun onLocationChanged(location: Location) {
        Log.i("Location", "onLocationChanged")
        longitude = location.longitude
        latitude = location.latitude
    }*/

    fun showStopperError(
        title: String,
        errorString: String,
        listener: DialogInterface.OnClickListener? = null,
        cancellable: Boolean = true
    ) {
        AlertDialog.Builder(this)
            .setTitle(title)
            .setMessage(errorString)
            .setPositiveButton(R.string.ok, listener)
            .setCancelable(cancellable)
            .show()
    }

    fun showStopperError(
        @StringRes title: Int,
        @StringRes errorString: Int,
        listener: DialogInterface.OnClickListener? = null,
        cancellable: Boolean = true
    ) {
        showStopperError(getString(title), getString(errorString), listener, cancellable)
    }

    private fun isLocationPermissionsGranted(): Boolean {
        return ActivityCompat.checkSelfPermission(
            this,
            Manifest.permission.ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED

    }
}