package com.sa.saheer.ui.login

import android.app.Activity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import com.sa.saheer.R
import com.sa.saheer.databinding.ActivityLoginBinding
import com.sa.saheer.ui.MainActivity
import com.sa.saheer.ui.base.BaseActivity
import com.sa.saheer.utils.EmpRole
import com.sa.saheer.utils.Status
import org.koin.androidx.viewmodel.ext.android.viewModel
import splitties.activities.start


class LoginActivity : BaseActivity<ActivityLoginBinding>() {

    private val loginViewModel: LoginViewModel by viewModel()

    override val bindingInflater: (LayoutInflater) -> ActivityLoginBinding
        get() = ActivityLoginBinding::inflate

    override fun initUi(savedInstanceState: Bundle?) {

        binding.etUserName.afterTextChanged {
            loginViewModel.loginDataChanged(
                binding.etUserName.text.toString(),
                binding.etPw.text.toString()
            )
        }

        binding.etPw.apply {
            afterTextChanged {
                loginViewModel.loginDataChanged(
                    binding.etUserName.text.toString(),
                    binding.etPw.text.toString()
                )
            }

            setOnEditorActionListener { _, actionId, _ ->
                when (actionId) {
                    EditorInfo.IME_ACTION_DONE ->
                        loginViewModel.login(
                            binding.etUserName.text.toString(),
                            binding.etPw.text.toString()
                        )
                }
                false
            }

            binding.login.setOnClickListener {
                binding.loading.visibility = View.VISIBLE
                loginViewModel.login(
                    binding.etUserName.text.toString(),
                    binding.etPw.text.toString()
                )

                //onLoginSuccess(LoggedInUserView("Mahmoud Shawky", EmpRole.SUPERVISOR.role))
            }
        }
    }

    override fun initObservers() {
        loginViewModel.userId.observe(this@LoginActivity, {
            it?.let { userId ->
                binding.etUserName.setText(userId)
                binding.etUserName.error = null
            }
        })

        loginViewModel.userPw.observe(this@LoginActivity, {
            it?.let { userPw ->
                binding.etPw.setText(userPw)
                binding.etPw.error = null
            }
        })

        loginViewModel.loginFormState.observe(this@LoginActivity, Observer {
            val loginState = it ?: return@Observer

            // disable login button unless both username / password is valid
            binding.login.isEnabled = loginState.isDataValid

            if (loginState.usernameError != null) {
                binding.etUserName.error = getString(loginState.usernameError)
            }
            if (loginState.passwordError != null) {
                binding.etPw.error = getString(loginState.passwordError)
            }
        })

        loginViewModel.loginResult.observe(this@LoginActivity, Observer {
            val loginResult = it ?: return@Observer

            binding.loading.visibility = View.GONE
            binding.login.visibility = View.VISIBLE
            if (loginResult.error != null) {
                showLoginFailed(loginResult.error)
            }
            if (loginResult.success != null) {
                onLoginSuccess(loginResult.success)
            }
        })

        loginViewModel.networkState.observe(this@LoginActivity, Observer {
            val state = it ?: return@Observer
            when (state.status) {
                Status.RUNNING -> {
                    showLoading(true)
                }
                Status.SUCCESS -> {
                    showLoading(false)
                }
                Status.FAILED -> {
                    showLoading(false)
                    state.msg?.let { it1 -> showLoginFailed(it1) }
                }
            }
        })
    }

    private fun showLoading(isShow: Boolean) {
        binding.loading.visibility = if (isShow) View.VISIBLE else View.GONE
        binding.login.visibility = if (isShow) View.GONE else View.VISIBLE
    }

    private fun onLoginSuccess(model: LoggedInUserView) {
        val welcome = getString(R.string.welcome)
        val displayName = model.displayName

        Toast.makeText(
            applicationContext,
            "$welcome $displayName",
            Toast.LENGTH_LONG
        ).show()

        setResult(Activity.RESULT_OK)

        start<MainActivity> {
            putExtras(bundleOf(MainActivity.KEY_EMP_TYPE to model.empRole))
        }
        finish()
    }

    private fun showLoginFailed(@StringRes errorString: Int) {
        Toast.makeText(applicationContext, errorString, Toast.LENGTH_SHORT).show()
    }

    private fun showLoginFailed(errorString: String) {
        Toast.makeText(applicationContext, errorString, Toast.LENGTH_SHORT).show()
    }
}

/**
 * Extension function to simplify setting an afterTextChanged action to EditText components.
 */
fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(editable: Editable?) {
            afterTextChanged.invoke(editable.toString())
        }

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
    })
}
