package com.sa.saheer.ui

import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.text.format.DateUtils
import android.util.Log
import android.view.LayoutInflater
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.NavHostFragment.findNavController
import com.instacart.library.truetime.TrueTime
import com.sa.saheer.R
import com.sa.saheer.databinding.ActivityMainBinding
import com.sa.saheer.ui.base.BaseActivity
import com.sa.saheer.utils.EmpRole
import com.sa.saheer.utils.MAX_TIME_DIFFERENCE
import com.sa.saheer.utils.interfaces.OnItemClickListener
import java.util.*
import kotlin.math.abs

class MainActivity : BaseActivity<ActivityMainBinding>() {

    private lateinit var myNavHostFragment: NavHostFragment
    private var navGraph: Int = R.navigation.nav_graph

    companion object {
        const val KEY_EMP_TYPE = "key_emp_type"
    }

    override val bindingInflater: (LayoutInflater) -> ActivityMainBinding
        get() = ActivityMainBinding::inflate

    override fun initUi(savedInstanceState: Bundle?) {
        setTitleBar()

        val empRole = intent.extras?.getString(KEY_EMP_TYPE)

        empRole?.let {
            navGraph = when (EmpRole.valueOf(it.toUpperCase(Locale.ENGLISH))) {
                EmpRole.GUARD -> R.navigation.nav_graph
                EmpRole.SUPERVISOR -> R.navigation.supervisor_nav_graph
                //EmpRole.ADMIN -> TODO()
                else -> return
            }

        }

        myNavHostFragment =
            supportFragmentManager.findFragmentById(R.id.navHostFragment) as NavHostFragment
        //val navController = myNavHostFragment.navController

        val inflater = myNavHostFragment.navController.navInflater
        val graph = inflater.inflate(navGraph)
        myNavHostFragment.navController.graph = graph
    }

    fun setTitleBar(
        @StringRes titleId: Int = R.string.app_title,
        backEnabled: Boolean = false
    ) {
        supportActionBar?.apply {
            /*displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM;
            setCustomView(R.layout.custom_app_bar);*/
            setDisplayHomeAsUpEnabled(backEnabled)
            title = getString(titleId)
        }
    }

    override fun onResume() {
        super.onResume()
        checkDeviceTime()
    }

    private fun checkDeviceTime() {
        val trueTime = TrueTime.now()
        val sysTime = System.currentTimeMillis()
        Log.i("Time", "True Time ${trueTime.time}")
        Log.i("Time", "System Time $sysTime")
        if (abs(trueTime.time - sysTime) > DateUtils.MINUTE_IN_MILLIS * MAX_TIME_DIFFERENCE) {
            invalidTimeDialog.show()
        } else {
            invalidTimeDialog.dismiss()
            requireLocation()
        }
    }

    private fun requireLocation() {
        askForLocationPermission(object : OnItemClickListener<Boolean> {
            override fun onItemClicked(allGranted: Boolean) {
                if (!allGranted) {
                    showStopperError(
                        R.string.error,
                        R.string.permission_fine_location_message,
                        cancellable = false,
                        listener = { dialog, _ ->
                            dialog.dismiss()
                            requireLocation()
                        }
                    )
                }
            }
        })
    }

    private val settingsIntent = Intent(Settings.ACTION_DATE_SETTINGS)
    private var startForResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { _ ->
            /*if (result.resultCode == Activity.RESULT_OK) {
                // There are no request codes
                val data: Intent? = result.data

            }*/
            checkDeviceTime()
        }

    private val invalidTimeDialog by lazy {
        AlertDialog.Builder(this)
            .setTitle(R.string.error)
            .setMessage(R.string.time_error).setIcon(android.R.drawable.stat_notify_error)
            .setCancelable(false)
            .setPositiveButton(R.string.ok) { _, _ ->
//                startActivity(settingsIntent)
                startForResult.launch(settingsIntent)
            }.create()
    }

    override fun onSupportNavigateUp(): Boolean {
         //super.onSupportNavigateUp()
        return myNavHostFragment.navController.navigateUp()
    }

}