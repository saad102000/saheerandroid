package com.sa.saheer.ui.dailyReport

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.sa.saheer.R
import com.sa.saheer.data.remote.models.AttendanceItem
import com.sa.saheer.utils.UI_DATE_FORMAT
import java.text.SimpleDateFormat
import java.util.*

class GuardsAttendanceAdapter(
    private val values: List<Pair<Long, List<AttendanceItem>>>
) : RecyclerView.Adapter<GuardsAttendanceAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_attendance, parent, false)
        return ViewHolder(view)
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        holder.apply {
            tvName.text = SimpleDateFormat(UI_DATE_FORMAT, Locale.getDefault()).format(item.first)
            rvAttendance.adapter = AttendanceItemAdapter(item.second)
        }
    }

    override fun getItemCount(): Int = values.size

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tvName: TextView = view.findViewById(R.id.tvName)
        val rvAttendance: RecyclerView = view.findViewById(R.id.rvAttendance)
    }
}