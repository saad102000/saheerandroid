package com.sa.saheer.ui.vacationRequest

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.sa.saheer.data.Repository
import com.sa.saheer.data.remote.Result
import com.sa.saheer.data.remote.models.LookupItem
import com.sa.saheer.data.remote.models.SubmitLeaveRequest
import com.sa.saheer.utils.NetworkState
import com.sa.saheer.utils.SERVER_DATE_TIME_FORMAT
import kotlinx.coroutines.launch
import com.sa.saheer.ui.base.BaseViewModel
import java.text.SimpleDateFormat
import java.util.*

class VacationRequestViewModel(private val repository: Repository) : BaseViewModel(repository) {

    private val _requestResult = MutableLiveData<Boolean>()
    val attendanceResult: LiveData<Boolean> get() = _requestResult

    private val _leaveTypes = MutableLiveData<ArrayList<LookupItem>>()
    val leaveTypes: LiveData<ArrayList<LookupItem>> get() = _leaveTypes

    init {
        getLeaveTypes()
    }

    fun getLeaveTypes() {
        viewModelScope.launch(handler) {
            _networkState.postValue(NetworkState.LOADING)
            when (val result = repository.getLeaveTypes()) {
                is Result.Success -> {
                    _networkState.postValue(NetworkState.LOADED)
                    result.data?.let {
                        _leaveTypes.postValue(it)
                    }

                }
                is Result.Error -> {
                    _networkState.postValue(NetworkState.error(result.exception.msg))
                    _leaveTypes.postValue(null)
                }
            }
        }
    }

    fun requestVacation(type: String, fromDate: Long, toDate: Long, note: String) {
        viewModelScope.launch(handler) {
            _networkState.postValue(NetworkState.LOADING)
            val start = SimpleDateFormat(SERVER_DATE_TIME_FORMAT, Locale.ENGLISH).format(fromDate)
            val end = SimpleDateFormat(SERVER_DATE_TIME_FORMAT, Locale.ENGLISH).format(toDate)
            val leaveRequest =
                SubmitLeaveRequest(startDate = start, endDate = end, type = type, note = note)
            when (val result = repository.submitLeave(repository.getUserId(), leaveRequest)) {
                is Result.Success -> {
                    _networkState.postValue(NetworkState.LOADED)
                    _requestResult.postValue(true)
                }
                is Result.Error -> {
                    _networkState.postValue(NetworkState.error(result.exception.msg))
                    _requestResult.postValue(false)
                }
            }
        }
    }
}